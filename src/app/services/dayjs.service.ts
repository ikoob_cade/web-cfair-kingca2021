import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc';
import * as isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import * as isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import 'dayjs/locale/ko';
import * as toObject from 'dayjs/plugin/toObject';
import { Injectable } from '@angular/core';

@Injectable()
export class DayjsService {
    constructor() {
        dayjs.extend(utc);
        dayjs.extend(toObject);
        dayjs.extend(isSameOrAfter);
        dayjs.extend(isSameOrBefore);
        dayjs.locale('ko'); // 한국 로케일
    }

    /** 현재 한국시간을 반환한다. */
    getNow(): dayjs.Dayjs {
        const now = dayjs.utc().add(9, 'hour'); // utc로 생성 후, 9시간 추가
        return now;
    }

    /**
     * 시간비교 함수 param1 <= param2
     * @param param1 첫번째 시간
     * @param param2 두번째 시간
     */
    isSameOrBefore(param1: dayjs.Dayjs, param2: dayjs.Dayjs): boolean {
        return param1.isSameOrBefore(param2);
    }

    /**
     * 시간비교 함수 param1 >= param2
     * @param param1 첫번째 시간
     * @param param2 두번째 시간
     */
    isSameOrAfter(param1: dayjs.Dayjs, param2: dayjs.Dayjs): boolean {
        return param1.isSameOrAfter(param2);
    }

    /**
     * 날짜 포맷팅
     * @param param dayjs 객체
     * @param includeDotw day of the week
     * @param short short name of the day of the week
     */
    format(param: dayjs.Dayjs, includeDotw?: boolean, short?: boolean): string {
        let Dotw;
        if (includeDotw) {
            if (short) {
                Dotw = ' ddd';
            } else {
                Dotw = ' dddd';
            }
        }
        return param.format(`YYYY-MM-DD HH:mm${Dotw ?? ''}`);
    }

    /** 오브젝트로 반환한다. */
    toObject(param: dayjs.Dayjs): any {
        return param.toObject();
    }
}
