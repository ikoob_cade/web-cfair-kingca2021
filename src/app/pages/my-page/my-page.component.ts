import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { MemberService } from '../../services/api/member.service';
import { SocketService } from '../../services/socket/socket.service';

declare var $: any;
enum Change_Password_Message {
  FILL_ALL = '모든 항목을 입력해 주세요.',
  FAILED_NEW_CONFIRM = '새 비밀번호 확인이 잘못 입력되었습니다.\n다시 확인해 주세요.',
  SUCCESS = '비밀번호가 변경되었습니다.'
}

@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit {
  public user: any;
  public histories: Array<any>;
  public historiesOfVod: Array<any>;

  public totalTime = null;
  public booths: [];
  public PASSWORD_NEW: string;
  public PASSWORD_CONFIRM: string;

  public hour;
  public hourOfVod;
  public min;
  public minOfVod;
  public secOfVod;

  public selectedHistory;

  constructor(
    public router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    if (!this.user) {
      this.router.navigate(['/main']);
    }

    this.getMyInfo();
    this.getMyHistory();
    // this.getMyHistoryOfVod();
    // this.getVisitBooth();
  }

  /** 사용자 정보 조회 */
  getMyInfo(): void {
    this.memberService.getMyInfo(this.user.id).subscribe(res => {
      if (res) {
        this.user = res;
      }
    });
  }

  /*
   * 사용자 시청기록 조회
   * live 기록의 total은 분 단위
   */
  getMyHistory(): void {
    this.memberService.getMyHistoryOfSession(this.user.id).subscribe(res => {
      if (res && res.logs) {
        if (res.totalTime < 60) {
          this.hour = '00';
        } else {
          this.hour = Math.floor(res.totalTime / 60).toString();
          if (this.hour.length < 2) {
            this.hour = '0' + this.hour;
          }
        }

        this.min = (res.totalTime % 60).toString();
        if (this.min.length < 2) {
          this.min = '0' + this.min;
        }
        this.histories = this.refineLogs(res.logs);
      }
    });
  }

  /*
   * 사용자 VOD 시청기록 조회
   * vod기록 total은 초단위로 받는다.
   */
  getMyHistoryOfVod(): void {
    this.memberService
      .getMyHistoryOfVod(
        this.user.id,
        '2021-08-20 15:00:00', '2021-08-21 15:00:00')
        // '2021-08-09 15:00:00', '2021-08-21 15:00:00')
      .subscribe(res => {
        if (res) {
          this.historiesOfVod = res.logs;

          const total = res.total;
          // let total = 3757; // test : 1시간 2분 37초
          if (total < 3600) {
            this.hourOfVod = '00';
          } else {
            this.hourOfVod = Math.floor(total / 3600).toString();
            if (this.hourOfVod.length < 2) {
              this.hourOfVod = '0' + this.hourOfVod;
            }
          }

          this.minOfVod = Math.floor(total % 3600 / 60).toString();

          if (this.minOfVod.length < 2) {
            this.minOfVod = '0' + this.minOfVod;
          }

          this.secOfVod = Math.floor(total % 3600 % 60).toString();
          if (this.secOfVod.length < 2) {
            this.secOfVod = '0' + this.secOfVod;
          }

          // watchTime to mm:ss
          for (const vod of this.historiesOfVod) {
            if (vod.watchTime) {
              const watchTime = new Date(0);
              watchTime.setHours(0);
              watchTime.setSeconds(vod.watchTime);
              vod.watchTime = watchTime;
              // vod.watchTime = new Date(0).setSeconds(vod.watchTime).toString();
            } else {
              vod.watchTime = '00:00';
            }
          }
        }
      });
  }

  refineLogs(logsOfDates): Array<any> {
    const histories = [];
    // tslint:disable-next-line: forin
    for (const i in logsOfDates) {
      let hour;
      let min;

      logsOfDates[i].datas = _.groupBy(logsOfDates[i].datas, 'roomName');

      if (logsOfDates[i].totalTime < 60) {
        hour = '00';
      } else {
        hour = Math.floor(logsOfDates[i].totalTime / 60).toString();
        if (hour.length < 2) {
          hour = '0' + hour;
        }
      }

      min = (logsOfDates[i].totalTime % 60).toString();
      if (min.length < 2) {
        min = '0' + min;
      }
      histories.push({ date: i, rooms: logsOfDates[i].datas, hour, min });
    }
    return histories;
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  /** 비밀번호 변경 */
  changePassword(): void {
    const errorMessage = this.passwordValidator();
    if (!errorMessage) {
      this.memberService.changePassword(this.user.id, this.PASSWORD_CONFIRM).subscribe(res => {
        alert('Your password has been changed.');
        this.cancelChangePwd();
      });
    } else {
      alert(errorMessage);
    }
  }

  /** 비밀번호 변경 취소 */
  cancelChangePwd(): void {
    $('#changePassword').modal('hide');
    this.PASSWORD_NEW = '';
    this.PASSWORD_CONFIRM = '';
  }

  /** 비밀번호 변경 유효성 검사 */
  passwordValidator(): string {
    if (!this.PASSWORD_NEW || !this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FILL_ALL;
    } else if (this.PASSWORD_NEW !== this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FAILED_NEW_CONFIRM;
    }
    return null;
  }

  /*
   * 부스 조회 (나의 부스방문 기록)
   */
  private getVisitBooth(): void {
    this.memberService.findVisitors(this.user.id)
      .subscribe((data: any) => {
        this.booths = data;
      });
  }
}
