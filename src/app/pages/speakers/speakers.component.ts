import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { CategoryService } from '../../services/api/category.service';
import { SpeakerService } from '../../services/api/speaker.service';

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  public speakers: Array<any> = [];
  public mobile: boolean = false;

  private data: Array<any> = [];

  public speakerCategories: Array<any> = [];

  constructor(
    private speakerService: SpeakerService,
    private categoryService: CategoryService,
  ) { }

  ngOnInit(): void {
    this.mobile = window.innerWidth < 768;
    this.loadCategories();
    // this.loadSpeakers();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers = () => {
    this.speakerService.find(false).subscribe(res => {
      this.speakers = res;
    });
  }

  loadCategories = () => {
    this.categoryService.find('speaker').subscribe(res => {
      _.forEach(res, category => {
        const speakers = _.sortBy(category.speakers, 'seq').filter(speaker => speaker.isShow);
        category.speakers = speakers;
      });

      this.speakerCategories = res;
    });
  }
}
