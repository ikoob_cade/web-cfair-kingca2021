import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/api/event.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutComponent implements OnInit {
  @Input('opt') opt: boolean = false;

  public event;
  // public description: string;

  constructor(
    private eventService: EventService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.loadEventInfo();
  }

  loadEventInfo = () => {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
    });
  }

}
