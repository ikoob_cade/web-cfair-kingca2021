import * as _ from 'lodash';
import { UAParser } from 'ua-parser-js';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from '../../../services/api/vod.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from '../../../services/api/banner.service';

declare let $: any;
@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit, AfterViewInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public banners = [];
  private user = JSON.parse(sessionStorage.getItem('cfair'));
  public vodId;

  public vod: any; // vod 데이터
  public newInput;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
    private bannerService: BannerService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodId = params.vodId;
        this.findVod(this.vodId);
        this.getBanners();
      }
    });
  }

  ngAfterViewInit(): void {
  }

  findVod(vodId): void {
    this.vodService.findOne(vodId).subscribe(vod => {
      this.vod = vod;
    });
  }

  // ! Banner Slider
  slidePrev(target): void {
    this[target].prev();
  }

  slideNext(target): void {
    this[target].next();
  }

  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  // 리스트로 돌아가기
  goBack(): void {
    this.location.back();
  }

  // ! HTTP
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (_.keys(res).length > 0) {
        res.vod.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
    });
  }

  // ! 댓글
  getVodComments(): void {
    this.vod.comments = { commentList: [], count: 0 };

    this.vodService.findComments(this.vod.id).subscribe(res => {
      this.vod.comments = res;
    });
  }

  /** 댓글 생성 */
  createComment(parentId?: string): void {
    const parser = new UAParser();
    const fullUserAgent = parser.getResult();

    if (parentId) {
      if (!$(`#cInput_${parentId}`)[0].value) {
        return alert('질문을 입력하세요.');
      }
    } else if (!this.newInput) {
      return alert('질문을 입력하세요.');
    }

    const body: any = {
      description: parentId ? $(`#cInput_${parentId}`)[0].value : this.newInput,
      memberId: this.user.id,
      parentId: parentId ? parentId : '',
      level: parentId ? 1 : 0,
      userAgent: JSON.stringify(fullUserAgent),
      browser: JSON.stringify(fullUserAgent.browser),
      device: JSON.stringify(fullUserAgent.device),
      engine: JSON.stringify(fullUserAgent.engine),
      os: JSON.stringify(fullUserAgent.os),
      ua: JSON.stringify(fullUserAgent.ua),
    };

    this.vodService.createComment(this.vod.id, body).subscribe(res => {
      this.newInput = '';
      this.getVodComments();
    });
  }

  /** 답글 Input 활성화 */
  openReply(selectedCommentId): void {
    const cInputWrapper = $(`#cInput_wrapper_${selectedCommentId}`)[0];
    if (cInputWrapper.style.display === 'none' || !cInputWrapper.style.display) {
      cInputWrapper.style.display = 'block';
      $(`#cInput_${selectedCommentId}`)[0].focus();
    } else {
      cInputWrapper.style.display = 'none';
    }
  }

  /** 댓글 수정 활성화 */
  modify(inputId): void {
    const input = $(`#${inputId}`)[0];
    input.disabled = false;
    input.focus();
  }

  /** 댓글 수정 */
  update(commentId, type): void {
    let input;
    if (type === 'child') {
      input = $(`#cInput_${commentId}`)[0];
    } else {
      input = $(`#pInput_${commentId}`)[0];
    }

    if (!input.value) {
      return alert('댓글을 입력하세요.');
    }

    const body = {
      description: input.value,
    };

    this.vodService.updateComment(this.vod.id, commentId, body).subscribe(res => {
      this.getVodComments();
    });
  }

  isModify(inputId): boolean {
    if ($(`#${inputId}`) && $(`#${inputId}`)[0]) {
      return !$(`#${inputId}`)[0].disabled;
    }
    return false;
  }

  cancel(): void {
    this.getVodComments();
  }

  /** 댓글을 삭제한다 */
  remove(commentId): void {
    if (confirm('댓글을 삭제하시겠습니까?')) {
      this.vodService.deleteComment(this.vod.id, commentId).subscribe(res => {
        this.getVodComments();
      });
    }
  }

  vodOnAir(isOnOff): void {
    const confirmMessage = isOnOff ? '시작하시겠습니까?' : '종료하시겠습니까?';
    if (confirm(confirmMessage)) {
      this.vodService.updateOnOff(this.vod.id, isOnOff).subscribe(res => {
        this.findVod(this.vod.id);
      }, error => {
        console.error(error);
      });
    }

  }
}
