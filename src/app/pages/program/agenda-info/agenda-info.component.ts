import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AgendaService } from 'src/app/services/api/agenda.service';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';

import { forkJoin as observableForkJoin } from 'rxjs';

@Component({
  selector: 'app-agenda-info',
  templateUrl: './agenda-info.component.html',
  styleUrls: ['./agenda-info.component.scss']
})
export class AgendaInfoComponent implements OnInit {

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록
  public agenda: any; // 선택한 Agenda 정보
  public selected: any = {
    date: null,
    room: null
  };

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public agendaService: AgendaService,
    public dateService: DateService,
    public roomService: RoomService,
  ) {
    this.doInit();
  }

  ngOnInit(): void { }

  public doInit() {
    const observables = [this.getAgenda(), this.getDates(), this.getRooms()];
    observableForkJoin(observables).subscribe(res => {
      this.agenda = res[0];
      this.dates = res[1];
      this.rooms = res[2];
      this.selected.date = this.dates.find(date => {
        return date.id === this.route.snapshot.params['dateId']
      });
      this.selected.room = this.rooms.find(room => {
        return room.id === this.route.snapshot.params['roomId']
      });
    });
  }

  /** 아젠다 상세 조회 */
  getAgenda() {
    return this.agendaService.findById(this.route.snapshot.params['agendaId']);
  }

  /** 날짜 목록 조회 */
  getDates() {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms() {
    return this.roomService.find();
  }

  /**
   * 날짜 / 룸 선택
   * @param date 
   * @param room 
   */
  setAgendaList(date: any, room: any) {
    this.router.navigate([`/live`], { queryParams: { dateId: date.id, roomId: room.id } });
  }

}
