import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }


  scrollToTable(): void {
    document.getElementsByTagName('app-time-table')[0].scrollIntoView();
  }
}
